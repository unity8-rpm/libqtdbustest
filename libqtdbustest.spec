Name:   		libqtdbustest
Version:    	r83
Release:    	1%{?dist}
Summary:    	Library for testing DBus interactions using Qt

Group:  		Libraries
License:    	LGPL-v3
URL:    		https://github.com/ubports/libqtdbustest
Source0:    	https://github.com/abhishek9650/libqtdbustest/archive/%{name}-%{version}.tar.gz
Patch1: 		SuicidalSleep.patch

BuildRequires:	cmake, cmake-extras, gmock-devel
BuildRequires:  gcovr, lcov, qt5-devel
BuildRequires:  python-dbusmock, gtest-devel
Requires:	    gmock, gcovr, lcov, qt5, python-dbusmock

%description
Library for testing DBus interactions using Qt

%global debug_package %{nil}

%prep
%setup -q
%patch1 -p1

%build
%cmake
make %{?_smp_mflags}


%install
%make_install


%files
%{_bindir}/qdbus-simple-test-runner
%{_includedir}/%{name}-1/%{name}/*.h
%{_libdir}/pkgconfig/%{name}-1.pc
%{_libdir}/%{name}.so.1.0.0
%{_libdir}/%{name}.so.1
%{_libdir}/%{name}.so
%{_libexecdir}/%{name}/watchdog
%{_datadir}/%{name}/*.conf



%changelog
* Mon Oct 1 2018 Abhishek Mudgal <abhishek.mudgal.59@gmail.com>
- Create a spec file


